## my-job-portal-api
In this repo I have created the restful api using nodejs, express and mysql

### Author : Pritam Prince

### `MySQL configuration`
Please export the my-job-portal-api.sql from your phpmyadmin panel

In the project directory, you can run:

### `npm install`

This will install the dependencies inside `node_modules`

### `node server.js` OR `nodemon start` OR `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.
