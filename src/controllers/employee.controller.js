'use strict';

const Employee = require('../models/employee.model');


exports.create = function(req, res) {
    const new_user = new Employee(req.body);
    //handles null error 
   if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        Employee.create(new_user, function(err, user) {
            if (err)
            res.send(err);
            res.json({error:false,message:"Employee added successfully!",data:user});
        });
    }
};

exports.signin = function(req, res) {
  Employee.signin( req.params, function(err, user) {
    if (err)
    res.send(err);
    res.json({ error:false, message: 'Employee logged in successfully' });
  });
};