const express = require('express')
const router = express.Router()
const employeeController = require('../controllers/employee.controller');


// Create a new employee
router.post('/', employeeController.create);

// Create a new employee
router.post('/signin', employeeController.signin);



module.exports = router