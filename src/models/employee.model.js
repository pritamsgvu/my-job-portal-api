'user strict';
var dbConn = require('./../../config/db.config');

//Employee object create
var Employee = function (user) {
    this.email = user.email;
    this.password = user.password; //todo - save encrypted data
    this.status = 1;
    this.created_at = new Date();
    this.updated_at = new Date();
};
Employee.create = function (newUser, result) {
    dbConn.query("INSERT INTO users set ?", newUser, function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        }
        else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};

Employee.signin = function (data, result) {
    dbConn.query("Select * from users where email = ? && password = ? ", [data.email, data.password], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};


module.exports= Employee;